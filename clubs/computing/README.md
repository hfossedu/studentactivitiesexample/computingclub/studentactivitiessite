# Computing Club

## Officers

* Ant - President
* Bat - Vice
* Cat - Treasurer

## Meetings

We meet the 24th of each month at noon.

[Our minutes are here](./notes)
